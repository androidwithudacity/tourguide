package in.srishti.tourguide;

/**
 * Created by Srishti.Gupta on 03-02-2018.
 */

public class Attraction {

    private String mName; // name of an attraction
    private int mImageResourceId; // image resource id of an attraction
    private String mDetails = NO_DETAILS; // details about an attraction
    private String mLocation = NO_LOCATION; // location of an attraction
    // flag to keep track of whether the image is to be displayed in the list
    private boolean mShowImage = false; // TRUE if the image is to be shown in list; FALSE otherwise

    // flag to keep track of whether the location is provided or not
    private static final String NO_LOCATION = "";
    // flag to keep track of whether the details are provided or not
    private static final String NO_DETAILS = "";

    // constructor
    public Attraction(String name, int imageId, String details) {
        mName = name;
        mImageResourceId = imageId;
        mDetails = details;
    }

    public Attraction(String name, int imageId, String details, boolean showImage) {
        mName = name;
        mImageResourceId = imageId;
        mDetails = details;
        mShowImage = showImage;
    }

    // constructor
    public Attraction(String name, int imageId, String details, String location) {
        mName = name;
        mImageResourceId = imageId;
        mDetails = details;
        mLocation = location;
    }

    /**
     * Method to get the name of an attraction
     * @return name of the attraction
     */
    public String getName() {
        return mName;
    }

    /**
     * Method to get the location of an attraction
     * @return location of the attraction
     */
    String getLocation() {
        return mLocation;
    }

    /**
     * Method to get the image resource id of an attraction
     * @return image resource id of the attraction
     */
    int getImageResourceId() {
        return mImageResourceId;
    }

    /**
     * Method to get the details about an attraction
     * @return details about the attraction
     */
    String getDetails() {
        return mDetails;
    }

    /**
     * Method to check whether location exists for an attraction
     * @return TRUE is location exists; FALSE otherwise
     */
    boolean hasLocation() {
        return !(mLocation.equals(NO_LOCATION));
    }

    /**
     * Method to determine if the image is to be displayed in the listview
     * @return TRUE if the image is to be displayed in listview; FALSE otherwise
     */
    boolean isImageDisplayed() { return mShowImage; }
}