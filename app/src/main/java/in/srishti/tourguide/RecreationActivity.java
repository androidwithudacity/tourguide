package in.srishti.tourguide;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

public class RecreationActivity extends AppCompatActivity {

    // array list containing recreational activities
    ArrayList<Attraction> mRecreationArrList = new ArrayList<Attraction>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recreation);

        // Creating list of recreational activities acting as datasource for the recreation listview
        mRecreationArrList.add(new Attraction(getString(R.string.recreation_jungleSafari_name),
                R.drawable.recreation_jungle_safari, getString(R.string.heritage_jungleSafari_details),
                getString(R.string.heritage_jungleSafari_location)));
        mRecreationArrList.add(new Attraction(getString(R.string.recreation_lightSoundShow_name),
                R.drawable.recreation_light_sound_show, getString(R.string.heritage_lightSoundShow_details),
                getString(R.string.heritage_lightSoundShow_location)));
        mRecreationArrList.add(new Attraction(getString(R.string.recreation_atvRide_name),
                R.drawable.recreation_atv_ride, getString(R.string.heritage_atvRide_details),
                getString(R.string.heritage_atvRide_location)));
        mRecreationArrList.add(new Attraction(getString(R.string.recreation_elephantSafari_name),
                R.drawable.recreation_elephant_safari, getString(R.string.heritage_elephantSafari_details),
                getString(R.string.heritage_elephantSafari_location)));
        mRecreationArrList.add(new Attraction(getString(R.string.recreation_zorbing_name),
                R.drawable.recreation_zorbing, getString(R.string.heritage_zorbing_details),
                getString(R.string.heritage_zorbing_location)));
        mRecreationArrList.add(new Attraction(getString(R.string.recreation_jaipurOnCycle_name),
                R.drawable.recreation_jaipur_on_cycle, getString(R.string.heritage_jaipurOnCycle_details),
                getString(R.string.heritage_jaipurOnCycle_location)));

        // Binding custom adapter to the recreation listview
        AttractionAdapter attrAdapter = new AttractionAdapter(this, mRecreationArrList);
        ListView recreationListView = findViewById(R.id.recreation_listview);
        recreationListView.setAdapter(attrAdapter);

        // Binding OnClickListener to the list view items in the recreation listview
        recreationListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Attraction itemClicked = mRecreationArrList.get(i);

                Intent detailsIntent = new Intent(RecreationActivity.this, DetailsActivity.class);

                Bundle attractionInfoBundle = new Bundle();
                attractionInfoBundle.putString(getString(R.string.name), itemClicked.getName());
                attractionInfoBundle.putInt(getString(R.string.image), itemClicked.getImageResourceId());
                attractionInfoBundle.putString(getString(R.string.location), itemClicked.getLocation());
                attractionInfoBundle.putString(getString(R.string.details), itemClicked.getDetails());
                detailsIntent.putExtras(attractionInfoBundle);

                startActivity(detailsIntent);
            }
        });
    }
}