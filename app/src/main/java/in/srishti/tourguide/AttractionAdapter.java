package in.srishti.tourguide;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Srishti.Gupta on 03-02-2018.
 */

public class AttractionAdapter extends ArrayAdapter<Attraction> {

    AttractionAdapter(Activity context, ArrayList<Attraction> attractionsArrList) {
        super(context, 0, attractionsArrList);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        // get event object specified at specific position in the list
        Attraction currentAttraction = getItem(position);

        View listItemView = convertView;
        // if a recycling view does not exist, inflate a new view
        if (listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(R.layout.layout_attraction_list_item, parent, false);
        }

        ImageView imageView = listItemView.findViewById(R.id.attraction_imageview);
        if (currentAttraction.isImageDisplayed()) {
            imageView.setImageResource(currentAttraction.getImageResourceId());
            imageView.setVisibility(View.VISIBLE); // setting visibility back since views are getting reused
        } else {
            imageView.setVisibility(View.GONE);
        }

        TextView nameTextView = listItemView.findViewById(R.id.attraction_name_textview);
        nameTextView.setText(currentAttraction.getName());

        TextView locationTextView = listItemView.findViewById(R.id.attraction_location_textview);
        if (currentAttraction.hasLocation()) { // display location if it exists for an attraction
            locationTextView.setText(currentAttraction.getLocation());
            locationTextView.setVisibility(View.VISIBLE); // setting visibility back since views are getting reused
        } else {
            locationTextView.setVisibility(View.GONE);
        }

        return listItemView;
    }
}