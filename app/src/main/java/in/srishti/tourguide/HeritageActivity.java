package in.srishti.tourguide;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

public class HeritageActivity extends AppCompatActivity {

    // array list containing heritages
    ArrayList<Attraction> mHeritageArrList = new ArrayList<Attraction>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_heritage);

        // Creating list of heritages acting as datasource for the heritage listview
        mHeritageArrList.add(new Attraction(getString(R.string.heritage_hawaMahal_name),
                R.drawable.heritage_hawa_mahal, getString(R.string.heritage_hawaMahal_details)));
        mHeritageArrList.add(new Attraction(getString(R.string.heritage_albertHall_name),
                R.drawable.heritage_albert_hall, getString(R.string.heritage_albertHall_details)));
        mHeritageArrList.add(new Attraction(getString(R.string.heritage_amerFort_name),
                R.drawable.heritage_amer_fort, getString(R.string.heritage_amerFort_details)));
        mHeritageArrList.add(new Attraction(getString(R.string.heritage_cityPalace_name),
                R.drawable.heritage_city_palace, getString(R.string.heritage_cityPalace_details)));
        mHeritageArrList.add(new Attraction(getString(R.string.heritage_jaigarh_name),
                R.drawable.heritage_jaigarh_fort, getString(R.string.heritage_jaigarh_details)));
        mHeritageArrList.add(new Attraction(getString(R.string.heritage_jalMahal_name),
                R.drawable.heritage_jal_mahal, getString(R.string.heritage_jalMahal_details)));
        mHeritageArrList.add(new Attraction(getString(R.string.heritage_jantarMantar_name),
                R.drawable.heritage_jantar_mantar, getString(R.string.heritage_jantarMantar_details)));
        mHeritageArrList.add(new Attraction(getString(R.string.heritage_nahargarhFort_name),
                R.drawable.heritage_nahargarh_fort, getString(R.string.heritage_nahargarhFort_details)));

        // Binding custom adapter to the heritage listview
        AttractionAdapter attrAdapter = new AttractionAdapter(this, mHeritageArrList);
        ListView heritageListView = findViewById(R.id.heritage_listview);
        heritageListView.setAdapter(attrAdapter);

        // Binding OnClickListener to the list view items in the heritage listview
        heritageListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Attraction itemClicked = mHeritageArrList.get(i);

                Intent detailsIntent = new Intent(HeritageActivity.this, DetailsActivity.class);

                Bundle attractionInfoBundle = new Bundle();
                attractionInfoBundle.putString(getString(R.string.name), itemClicked.getName());
                attractionInfoBundle.putInt(getString(R.string.image), itemClicked.getImageResourceId());
                attractionInfoBundle.putString(getString(R.string.details), itemClicked.getDetails());
                detailsIntent.putExtras(attractionInfoBundle);

                startActivity(detailsIntent);
            }
        });
    }
}