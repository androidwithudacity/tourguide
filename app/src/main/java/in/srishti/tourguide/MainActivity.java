package in.srishti.tourguide;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Setting OnClickListener to the textviews on home screen
        TextView recreationTextView = findViewById(R.id.events_textview);
        recreationTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent recreationIntent = new Intent(MainActivity.this, RecreationActivity.class);
                startActivity(recreationIntent);
            }
        });

        TextView heritageTextView = findViewById(R.id.heritage_textview);
        heritageTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent heritageIntent = new Intent(MainActivity.this, HeritageActivity.class);
                startActivity(heritageIntent);
            }
        });

        TextView cuisineTextView = findViewById(R.id.cuisine_textview);
        cuisineTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent cuisineIntent = new Intent(MainActivity.this, CuisineActivity.class);
                startActivity(cuisineIntent);
            }
        });

        TextView shoppingTextView = findViewById(R.id.shopping_textview);
        shoppingTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent shoppingIntent = new Intent(MainActivity.this, ShoppingActivity.class);
                startActivity(shoppingIntent);
            }
        });
    }
}