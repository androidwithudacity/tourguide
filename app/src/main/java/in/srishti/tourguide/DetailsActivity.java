package in.srishti.tourguide;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class DetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        // Setting values for views on the details activity
        ImageView imageView = findViewById(R.id.details_imageview);
        int imageId = getIntent().getExtras().getInt(getString(R.string.image));
        imageView.setImageResource(imageId);

        TextView nameTextView = findViewById(R.id.details_name_textview);
        String name = getIntent().getExtras().getString(getString(R.string.name));
        nameTextView.setText(name);

        TextView locationTextView = findViewById(R.id.details_location_textview);
        String location = getIntent().getExtras().getString(getString(R.string.location));
        if (location != null) { // show location only if it is passed
            locationTextView.setText("Location: " + location);
        } else {
            locationTextView.setVisibility(View.GONE);
        }

        TextView descriptionTextView = findViewById(R.id.details_description_textview);
        String details = getIntent().getExtras().getString(getString(R.string.details));
        descriptionTextView.setText(details);
    }
}