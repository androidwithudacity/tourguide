package in.srishti.tourguide;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

public class ShoppingActivity extends AppCompatActivity {

    // array list containing shopping places
    ArrayList<Attraction> mShoppingArrList = new ArrayList<Attraction>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping);

        // Creating list of shopping sites acting as datasource for the shopping listview
        mShoppingArrList.add(new Attraction(getString(R.string.shopping_badiChaupar_name),
                R.drawable.shopping_badi_chaupar, getString(R.string.shopping_badiChaupar_details)));
        mShoppingArrList.add(new Attraction(getString(R.string.shopping_rajaPark_name),
                R.drawable.shopping_raja_park, getString(R.string.shopping_rajaPark_details)));
        mShoppingArrList.add(new Attraction(getString(R.string.shopping_bapuBazaar_name),
                R.drawable.shopping_bapu_bazaar, getString(R.string.shopping_bapuBazaar_details)));
        mShoppingArrList.add(new Attraction(getString(R.string.shopping_chandpoleBazaar_name),
                R.drawable.shopping_chandpole_bazaar, getString(R.string.shopping_chandpoleBazaar_details)));
        mShoppingArrList.add(new Attraction(getString(R.string.shopping_johriBazaar_name),
                R.drawable.shopping_johri_bazaar, getString(R.string.shopping_johriBazaar_details)));
        mShoppingArrList.add(new Attraction(getString(R.string.shopping_kishanpoleBazaar_name),
                R.drawable.shopping_kishanpole_bazaar, getString(R.string.shopping_kishanpoleBazaar_details)));
        mShoppingArrList.add(new Attraction(getString(R.string.shopping_mirzaIsmailRoad_name),
                R.drawable.shopping_mirza_ismail_road, getString(R.string.shopping_mirzaIsmailRoad_details)));
        mShoppingArrList.add(new Attraction(getString(R.string.shopping_nehruBazaar_name),
                R.drawable.shopping_nehru_bazaar, getString(R.string.shopping_nehruBazaar_details)));
        mShoppingArrList.add(new Attraction(getString(R.string.shopping_tripoliaBazaar_name),
                R.drawable.shopping_tripolia_bazaar, getString(R.string.shopping_tripoliaBazaar_details)));
        mShoppingArrList.add(new Attraction(getString(R.string.shopping_sirehDeoliGate_name),
                R.drawable.shopping_sireh_deori_gate, getString(R.string.shopping_sirehDeoliGate_details)));

        // Binding custom adapter to the shopping listview
        AttractionAdapter attrAdapter = new AttractionAdapter(this, mShoppingArrList);
        ListView shoppingListView = findViewById(R.id.shopping_listview);
        shoppingListView.setAdapter(attrAdapter);

        // Binding OnClickListener to the list view items in the shopping listview
        shoppingListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Attraction itemClicked = mShoppingArrList.get(i);

                Intent detailsIntent = new Intent(ShoppingActivity.this, DetailsActivity.class);

                Bundle attractionInfoBundle = new Bundle();
                attractionInfoBundle.putString(getString(R.string.name), itemClicked.getName());
                attractionInfoBundle.putInt(getString(R.string.image), itemClicked.getImageResourceId());
                attractionInfoBundle.putString(getString(R.string.details), itemClicked.getDetails());
                detailsIntent.putExtras(attractionInfoBundle);

                startActivity(detailsIntent);
            }
        });
    }
}