package in.srishti.tourguide;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

public class CuisineActivity extends AppCompatActivity {

    ArrayList<Attraction> mCuisineArrList = new ArrayList<>(); // array list containing cuisines

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cuisine);

        // Creating list of cuisines acting as datasource for the cuisine listview
        mCuisineArrList.add(new Attraction(getString(R.string.cuisine_dalBaatiChoorma_name),
                R.drawable.cuisine_dal_baati_choorma, getString(R.string.cuisine_dalBaatiChoorma_details), true));
        mCuisineArrList.add(new Attraction(getString(R.string.cuisine_bajreKiRoti_name),
                R.drawable.cuisine_bajre_ki_roti, getString(R.string.cuisine_bajreKiRoti_details), true));
        mCuisineArrList.add(new Attraction(getString(R.string.cuisine_gajak_name),
                R.drawable.cuisine_gajak, getString(R.string.cuisine_gajak_details), true));
        mCuisineArrList.add(new Attraction(getString(R.string.cuisine_gatteKiSabji_name),
                R.drawable.cuisine_gatte_ki_sabji, getString(R.string.cuisine_gatteKiSabji_details), true));
        mCuisineArrList.add(new Attraction(getString(R.string.cuisine_ghevar_name),
                R.drawable.cuisine_ghevar, getString(R.string.cuisine_ghevar_details), true));
        mCuisineArrList.add(new Attraction(getString(R.string.cuisine_kerSangri_name),
                R.drawable.cuisine_ker_sangri, getString(R.string.cuisine_kerSangri_details), true));
        mCuisineArrList.add(new Attraction(getString(R.string.cuisine_lassi_name),
                R.drawable.cuisine_lassi, getString(R.string.cuisine_lassi_details), true));
        mCuisineArrList.add(new Attraction(getString(R.string.cuisine_mawaKachori_name),
                R.drawable.cuisine_mawa_kachori, getString(R.string.cuisine_mawaKachori_details), true));
        mCuisineArrList.add(new Attraction(getString(R.string.cuisine_mirchiWada_name),
                R.drawable.cuisine_mirchi_wada, getString(R.string.cuisine_mirchiWada_details), true));
        mCuisineArrList.add(new Attraction(getString(R.string.cuisine_pyaazKachori_name),
                R.drawable.cuisine_pyaaz_kachori, getString(R.string.cuisine_pyaazKachori_details), true));

        // Binding custom adapter to the cuisine listview
        AttractionAdapter attrAdapter = new AttractionAdapter(this, mCuisineArrList);
        ListView cuisineListView = findViewById(R.id.cuisine_listview);
        cuisineListView.setAdapter(attrAdapter);

        // Binding OnClickListener to the list view items in the cuisine listview
        cuisineListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Attraction itemClicked = mCuisineArrList.get(i);

                Intent detailsIntent = new Intent(CuisineActivity.this, DetailsActivity.class);

                Bundle attractionInfoBundle = new Bundle();
                attractionInfoBundle.putString(getString(R.string.name), itemClicked.getName());
                attractionInfoBundle.putInt(getString(R.string.image), itemClicked.getImageResourceId());
                attractionInfoBundle.putString(getString(R.string.details), itemClicked.getDetails());
                detailsIntent.putExtras(attractionInfoBundle);

                startActivity(detailsIntent);
            }
        });
    }
}